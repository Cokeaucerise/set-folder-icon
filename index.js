var fs = require('fs'),
  path = require('path'),
  util = require('util'),
  exec = require('child_process').exec,
  glob = require('glob'),
  numberStringRepresentation = require('number-string-representation');

var animeFoler = process.argv[2];
var animeImageFolder = process.argv[3];

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath).filter(function (folder) {

    var folderPath = path.join(srcpath, folder);
    var folderSanitize = folder.replace(/[^a-z0-9\ \']/ig, '').replace(/\..+$/, '');
    var folderWords = folderSanitize.split(' ');

    if (!fs.statSync(folderPath).isDirectory())
      return;

    var regex = "+(";

    folderWords
      .forEach(function (element) {
        if (!isNaN(element))
          element = `@(${element}|${numberStringRepresentation(parseFloat(element)).replace(/\ and\ .*/, '')})`
        else if (element.length <= 3)
          return;
        regex += `*${element}*|`
      }, this);

    regex = regex.replace(/\|$/, '');
    regex += ")";

    var files = glob(path.join(animeImageFolder, `${regex}.png`), { nocase: true }, function (err, files) {
      if (files.length > 0) {

        filesNoMatchCountDict = {};

        files.forEach(function (file) {

          var noMatchCount = 0;
          var fileNameWords = path.basename(file).replace(/\..+$/, '').replace(/[^a-zA-Z0-9\ \']/g, '').split(' ');

          fileNameWords.forEach(function (word) {
            noMatchCount += folderWords.indexOf(word) == -1;
          })

          if (!filesNoMatchCountDict[noMatchCount])
            filesNoMatchCountDict[noMatchCount] = [];

          filesNoMatchCountDict[noMatchCount].push(file);
        }, this)

        var bestChoicesIndex = Object.keys(filesNoMatchCountDict)[0];
        var bestChoice = filesNoMatchCountDict[bestChoicesIndex][0];

        console.log(`setting: ${bestChoice} for: ${folder}`)
        exec(`gvfs-set-attribute -t string ${folderPath.replace(/\ /g, '\\ ')} metadata::custom-icon file://${bestChoice.replace(/\ /g, '%20')}`);
      }
    });

  });
}

getDirectories(animeFoler);